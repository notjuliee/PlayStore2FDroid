Generate config:

```
./gen_config.py -e 'email@gmail.com' -p 'password'
```

Generate keystore:

```
keytool -genkey -v -keystore repo.jks -alias repokey -keyalg RSA -keysize 2048 -validity 10000
```
