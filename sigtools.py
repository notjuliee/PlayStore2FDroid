from subprocess import run, PIPE
from os import environ
from binascii import hexlify

def sign_jar(jar: str, keystore: str, alias: str, password: str):
    cmd = ["jarsigner", "-keystore", keystore, "-storepass:env", "PS2FD_KEY_PASS",
                "-digestalg", "SHA1", "-sigalg", "SHA1withRSA", jar, alias]
    do_env = environ.copy()
    do_env["PS2FD_KEY_PASS"] = password
    run(cmd, env=do_env)

def get_key(keystore: str, alias: str, password: str):
    cmd = ["keytool", "-exportcert", "-alias", alias,
            "-storepass:env", "PS2FD_KEY_PASS", "-keystore", keystore]
    do_env = environ.copy()
    do_env["PS2FD_KEY_PASS"] = password
    r = run(cmd, env=do_env, stdout=PIPE)
    return hexlify(r.stdout).decode("UTF-8")
