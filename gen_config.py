#!/usr/bin/env python
from gpapi.googleplay import GooglePlayAPI

from argparse import ArgumentParser
from json import dumps

ap = ArgumentParser(description="Generate config file for PlayStore2FDroid")
ap.add_argument("-e", "--email", dest="email", help="Google Email")
ap.add_argument("-p", "--password", dest="password", help="Google Password")

args = ap.parse_args()

server = GooglePlayAPI("en_US", "America/Los_Angeles")

server.login(args.email, args.password, None, None)

DEFAULT_CONF = {
    "repoName": "PlayStore2FDroid",
    "repoDesc": "Mirror of Play Store applications",
    "keystoreAlias": "repokey",
    "keystorePassword": "password",
    "watchPackageIds": [],
    "locale": "en_US",
    "timezone": "America/Los_Angeles",
    "gsfId": server.gsfId,
    "authSubToken": server.authSubToken
}

with open("config.json", "w+") as f:
    f.write(dumps(DEFAULT_CONF, indent=4))
