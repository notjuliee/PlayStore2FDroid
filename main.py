#!/usr/bin/env python
from gpapi.googleplay import GooglePlayAPI
from sigtools import sign_jar, get_key

import xml.etree.cElementTree as ET

from argparse import ArgumentParser
from sys import exit
from json import loads, dumps
from os import path, makedirs, chdir, getcwd
from datetime import datetime
from hashlib import sha256
from zipfile import ZipFile

try:
    from urllib import urlretrieve
except:
    from urllib.request import urlretrieve

ap = ArgumentParser(description="Convert Play Store apps to F-Droid repo")
ap.add_argument("path", help="Path to repo folder")
ap.add_argument("-c", "--config-file", help="Config File", default="config.json")
ap.add_argument("-k", "--key-store", help="Java KeyStore File", default="repo.jks")

args = ap.parse_args()

if not path.isfile(args.config_file):
    print("Config file not found!")
    exit(1)

if not path.isfile(args.key_store):
    print("KeyStore not found!")
    exit(1)

DAT_FILE = path.join(args.path, "ps2fd.json")

try:
    makedirs(path.join(args.path, "icons"))
except:
    pass

if not path.isfile(DAT_FILE):
    open(DAT_FILE, "w+").write("{}")

CONFIG = loads(open(args.config_file).read())
DAT = loads(open(DAT_FILE).read())
ISO_DATE = datetime.strftime(datetime.now(), "%Y-%m-%d")

server = GooglePlayAPI(CONFIG["locale"], CONFIG["timezone"])

server.login(None, None, CONFIG["gsfId"], CONFIG["authSubToken"])

app_info = server.bulkDetails(CONFIG["watchPackageIds"])

PUB_KEY = get_key(args.key_store, CONFIG["keystoreAlias"], CONFIG["keystorePassword"])

root = ET.Element("fdroid")
repo = ET.SubElement(root, "repo", icon="repo-icon.png", name=CONFIG["repoName"], pubkey=PUB_KEY)
ET.SubElement(repo, "description").text = CONFIG["repoDesc"]

print(f"Downloading {len(app_info)} apks")

for x, i in enumerate(app_info):
    if not i:
        continue

    if i["docId"] not in DAT:
        DAT[i["docId"]] = {
            "latestVersion": i["files"][-1]["version"],
            "added": ISO_DATE,
            "lastDate": ISO_DATE
        }

    if i["files"][-1]["version"] > DAT[i["docId"]]["latestVersion"]:
        DAT[i["docId"]]["latestVersion"] = i["files"][-1]["version"]
        DAT[i["docId"]]["lastDate"] = ISO_DATE

    filename = "{}.{}.apk".format(i["docId"], i["files"][-1]["version"])
    thumb_filename = "{}.{}.png".format(i["docId"], i["files"][-1]["version"])
    thumbnail = i["images"][0]

    if not path.isfile(path.join(args.path, "icons", thumb_filename)):
        print(f"Downloading thumbnail for {i['title']}")
        urlretrieve(thumbnail["url"], path.join(args.path, "icons", thumb_filename))

    fi = DAT[i["docId"]]
    
    app = ET.SubElement(root, "application", id=i["docId"])
    ET.SubElement(app, "id").text = i["docId"]
    ET.SubElement(app, "added").text = DAT[i["docId"]]["added"]
    ET.SubElement(app, "lastupdated").text = fi["lastDate"]
    ET.SubElement(app, "name").text = i["title"]
    ET.SubElement(app, "summary")
    ET.SubElement(app, "icon").text = thumb_filename
    ET.SubElement(app, "desc").text = "Mirrored from Play Store"
    ET.SubElement(app, "license").text = "Unknown"
    ET.SubElement(app, "categories").text = CONFIG["repoName"]
    ET.SubElement(app, "category").text = CONFIG["repoName"]
    ET.SubElement(app, "web")
    ET.SubElement(app, "source")
    ET.SubElement(app, "tracker")
    ET.SubElement(app, "changelog")
    ET.SubElement(app, "author").text = i["author"]
    ET.SubElement(app, "marketversion")
    ET.SubElement(app, "marketvercode").text = str(i["files"][-1]["version"])

    if not path.isfile(path.join(args.path, filename)):
        print(f"Download app {x}: {filename}")
        fl = server.download(i["docId"])
        with open(path.join(args.path, filename), "wb+") as f:
            for chunk in fl.get("file").get("data"):
                f.write(chunk)

    hashr = sha256()
    with open(path.join(args.path, filename), "rb") as f:
        while True:
            dat = f.read(16384)
            if len(dat) == 0:
                break
            hashr.update(dat)

    pkg = ET.SubElement(app, "package")
    ET.SubElement(pkg, "version").text = str(i["files"][-1]["version"])
    ET.SubElement(pkg, "versioncode").text = str(i["files"][-1]["version"])
    ET.SubElement(pkg, "apkname").text = filename
    ET.SubElement(pkg, "hash", type="sha256").text = hashr.hexdigest()
    ET.SubElement(pkg, "size").text = str(i["files"][-1]["size"])
    ET.SubElement(pkg, "added").text = fi["lastDate"]

print("Generating index")

tree = ET.ElementTree(root)
tree.write(path.join(args.path, "index.xml"))
open(DAT_FILE, "w+").write(dumps(DAT))

OLD_DIR = getcwd()
chdir(args.path)
ZipFile("index.jar", "w").write("index.xml")
chdir(OLD_DIR)
sign_jar(path.join(args.path, "index.jar"), args.key_store, CONFIG["keystoreAlias"], CONFIG["keystorePassword"])
